var gulp = require('gulp'),
replace = require('gulp-replace'),
runSequence = require('gulp-run-sequence'),
zip = require('gulp-zip'),
sshConfig = require('../sshConfig.js'),
clean = require('gulp-clean');

var distPath = 'build/';
var remoteZipPath = '/var/lib/tomcat7/webapps/emlaas-v2.zip';
var remoteProjectPath = '/var/lib/tomcat7/webapps/emlaas-v2';
var remoteZipPathForDocker = '/root/emlaas-react.zip';
var remoteProjectPathForDocker = '/root/emlaas-react';

//'/usr/share/nginx/html/emlaas'


gulp.task('cleanDist', function () {
  return gulp.src('dist')
  .pipe(clean());
});

gulp.task('replaceServerUrl', function () {
  var dirPath = distPath;
  return gulp.src(dirPath + 'bundle.js')
  .pipe(replace(/var SERVER_URL =(.+)/g, 'var SERVER_URL = "http://125.227.151.24";'))
  .pipe(gulp.dest(dirPath));
});

gulp.task('replaceNodeUrl', function () {
  var dirPath = distPath;
  return gulp.src(dirPath + 'bundle.js')
      .pipe(replace(/var NODE_URL =(.+)/g, 'var NODE_URL = "http://125.227.151.24:8888";'))
      .pipe(gulp.dest(dirPath));
});

gulp.task('replaceServerUrlForDocker', function () {
  var dirPath = distPath;
  return gulp.src(dirPath + 'bundle.js')
  .pipe(replace(/var SERVER_URL =(.+)/g, 'var SERVER_URL = "http://172.20.1.98:48887";'))
  .pipe(gulp.dest(dirPath));
});

gulp.task('replaceNodeUrlForDocker', function () {
  var dirPath = distPath;
  return gulp.src(dirPath + 'bundle.js')
  .pipe(replace(/var NODE_URL =(.+)/g, 'var NODE_URL = "http://172.20.1.98:48888";'))
  .pipe(gulp.dest(dirPath));
});

gulp.task('zipDist', function () {
  return gulp.src(distPath + '**')
  .pipe(zip('emlaas-v2.zip'))
  .pipe(gulp.dest('dist'));
});

gulp.task('zipDistForDocker', function () {
  return gulp.src(distPath + '**')
  .pipe(zip('emlaas-react.zip'))
  .pipe(gulp.dest('dist'));
});

gulp.task('deleteRemoteFiles', function () {
  return ssh.exec(['rm -rf ' + remoteProjectPath], {filePath: 'deleteRemoteFiles.log'})
  .pipe(gulp.dest('dist/logs'));
});

gulp.task('deleteRemoteFilesForDocker', function () {
  return ssh.exec(['rm -rf ' + remoteProjectPathForDocker], {filePath: 'deleteRemoteFiles.log'})
  .pipe(gulp.dest('dist/logs'));
});

gulp.task('uploadZip', function () {
  return gulp.src('dist/emlaas-v2.zip')
  .pipe(ssh.sftp('write', remoteZipPath));
});

gulp.task('uploadZipForDocker', function () {
  return gulp.src('dist/emlaas-react.zip')
  .pipe(ssh.sftp('write', remoteZipPathForDocker));
});

gulp.task('unzipRemoteFiles', function () {
  return ssh.exec(['unzip ' + remoteZipPath + ' -d ' + remoteProjectPath], {filePath: 'unzipRemoteFiles.log'})
  .pipe(gulp.dest('dist/logs'));
});

gulp.task('unzipRemoteFilesForDocker', function () {
  return ssh.exec(['unzip ' + remoteZipPathForDocker + ' -d ' + remoteProjectPathForDocker], {filePath: 'unzipRemoteFiles.log'})
  .pipe(gulp.dest('dist/logs'));
});

gulp.task('deleteRemoteZip', function () {
  return ssh.exec(['rm ' + remoteZipPath], {filePath: 'deleteRemoteZip.log'})
  .pipe(gulp.dest('dist/logs'));
});

gulp.task('deleteRemoteZipForDocker', function () {
  return ssh.exec(['rm ' + remoteZipPathForDocker], {filePath: 'deleteRemoteZip.log'})
  .pipe(gulp.dest('dist/logs'));
});

gulp.task('deploy-emlaas', function (cb) {
  setDeployServer("emlaas");
  runSequence(
    'cleanDist',
    'replaceServerUrl',
    'zipDist',
    'deleteRemoteFiles',
    'uploadZip',
    'unzipRemoteFiles',
    'deleteRemoteZip',
    cb);
  });

  gulp.task('deploy-emlaas-docker', function (cb) {
    setDeployServer("emlaas-docker");
    runSequence(
      'cleanDist',
      'replaceServerUrlForDocker',
      'zipDistForDocker',
      'deleteRemoteFilesForDocker',
      'uploadZipForDocker',
      'unzipRemoteFilesForDocker',
      'deleteRemoteZipForDocker',
      cb);
    });


    function setSSHConfig(config) {
      ssh = require('gulp-ssh')({
        ignoreErrors: false,
        sshConfig: config
      });
    }

    function setDeployServer(serverName) {
      switch (serverName) {
        case "emlaas-docker":
        setSSHConfig(sshConfig.emlaas_docker);
        break;
        case "emlaas":
        setSSHConfig(sshConfig.emlaas);
        break;
        case "aws":
        setSSHConfig(sshConfig.aws);
        break;
        case "kiwi":
        setSSHConfig(sshConfig.kiwi);
        break;
      }
    }
