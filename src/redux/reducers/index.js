import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'


import {createModalState, detailModalState, animalTmp, animals} from './zoo.js'


export default combineReducers({
  zoo: combineReducers({
    createModalState,
    detailModalState,
    animalTmp,
    animals
  }),
  routing: routerReducer
});
