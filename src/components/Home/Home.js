import React, { PropTypes } from 'react'
import {Button, Col, Image} from 'react-bootstrap'

import './Home.css'

export default class Home extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="container">
        <h1>Yoyo's Zoos</h1>
        <img src={require("../../public/img/bk.jpg")}/>
        <h2>YOYO 動物園位於天龍市文山區萬興里</h2>
        <p>全園佔地面積計165公頃，創立於1914年日據時期，舊址位於龜山，為官營動物園</p>
        <p>當時展出以鳥類、哺乳類和爬蟲類為主，最著名的動物為一隻紅毛猩猩，死後也製成標本</p>
        <p>可惜在搬遷中已破損；到了第二次世界大戰，因為戰爭的摧毀，動物園的第一階段也告一段落</p>
      </div>
    )
  }
}
